const mongoose = require("mongoose");

const userSchema = mongoose.Schema({
  name:{
    type: String,
    required: true,
  },
  isCustomer: {
    type:Boolean,
    required: true,
  },
});

module.exports = mongoose.model("Usermodel", userSchema);
