const User = require("../models/Usermodel");

exports.registerUser = async (req, res) => {
  try {
    const user = new User({
      name: req.body.name,
      isCustomer: req.body.isCustomer,
    });
    await user.save();
    res.status(200).json({
      Message: `You just got registered! Welcome.`,
      username: req.body.username,
    });
  } catch (error) {
    res.status(400).json({ Message: "Error registering user." });
  }
};
exports.deleteUser = async (req, res) => {
  try {
    // Get the user ID from the request query parameters
    const userId = req.query.id;

    // Delete the user with the specified ID
    await User.deleteOne({ _id: userId });

    res.status(200).json({ Message: `User with ID ${userId} deleted.` });
  } catch (error) {
    res.status(400).json({ Message: error });
  }
};

exports.getAllUsers = async (req, res) => {
  try {
    // Get all users from the database
    const users = await User.find();

    res.status(200).json({ users });
  } catch (error) {
    res.status(400).json({ Message: error });
  }
};
